
var User = function (name) {
    this.name = name;

    this.sayMyName = function () {
        console.log("User: " + this.name);
    };
}

function test() {
    var user = new User("Kelly");
    user.sayMyName();
}


// test()


var DecoratedUser = function (user, street, city) {
    this.user = user;
    this.name = user.name;  // ensures interface stays the same
    this.street = street;
    this.city = city;

    this.sayMyName = function () {
        console.log("Decorated User: " + this.name + ", " +
            this.street + ", " + this.city);
    };
}

function run() {
    var user = new User("Kelly");
    user.sayMyName();
    var decorated = new DecoratedUser(user, "Broadway", "New York");
    decorated.sayMyName();
    userList = [user, decorated]
    userList.forEach(element => {
        element.sayMyName()
    });
}
run()