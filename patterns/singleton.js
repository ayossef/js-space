class Validator{
    isEmpty(str){
        return !str || (str === "") 
    }
}
var Singleton = (function () {
    var instance;
    function createInstance() {
        var object = new Validator();
        return object;
    }
    return {
        getInstance: function () { // public space
            if (!instance) {
                instance = createInstance(); // we can access private space
            }
            return instance;
        }
    };
})();


var FirstClass = function () {
    var pvtVar;
    function create(){
       pvtVar = "Secret"
    }
    return{
        pblcVar : 0,
        pubFunction: function () {console.log("Public Context");},
        getInstance: function () {
            if(!pvtVar){
                create()
            }
            return pvtVar
        }
    }
};


function run() {

    var instance1 = Singleton.getInstance();
    var instance2 = Singleton.getInstance();

    console.log("Same instance? " + (instance1 === instance2));
    console.log(instance1.isEmpty(""))
    console.log(instance2.isEmpty(null))
    console.log(FirstClass().pubFunction())
    console.log(FirstClass().getInstance())
}

run()