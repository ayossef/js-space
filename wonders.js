data = {
    name: "John",
    favCountries:["ZA","EG","AE","UK"],
    location: {
        city: "Johansberg",
        building: 583,
        St: 102
    },
    print: function (){ console.log(name+" loves countries :"+favCountrier)}
}

// JSON : Javascript Object Notation
console.log(data)
console.log(data.favCountries)

const x = 10
console.log(x)

const y = function() {console.log("Hello")}
y()

const z = function () {return "Hello from the Z side"}
console.log(z())



console.log("===========")
function f1(){
    console.log("this is First Function")
}
f2 = function(){
    console.log("this is the Second function")
    return f1
}
function execute(f){
    f()
}
execute(f2())

function separator() {
    console.log("=======")
}

separator()
function simpleFun(){
    console.log("Simple")
    return separator
}
console.log(simpleFun)
console.log(simpleFun())


separator()
mycar = {
    brand: "Toyota",
    color: "white",
    speed: 0,
    speedUp: function(){this.speed += 20}
}
console.log(mycar.brand)
console.log(mycar.speed)
mycar.speedUp()
console.log(mycar.speed)
console.log(mycar.prototype)

mydinner = {
    side: "Fries",
    main:"Chicken",
    drink: "Tea",
    order: function (){
        var orderTime = "tonight"
        console.log("Odrder is: \n"+this.main+" - "+this.side+" and finally, "+this.drink)
        return orderTime
    },
    pay: function(){
        console.log("Thanks for payment, hope you have enjoyed our "+this.main +","+ this.order())
    }

}

yourdinner = {
    side: "Fries",
    main:"Chicken",
    drink: "Tea"
}
class Dinner{
    constructor(side,main,drink){
        this.side = side
        this.main = main
        this.drink = drink
    }
}
hisDinner = new Dinner("Fries","Meat","Coke")
herDinner = new Dinner("Salad","Shrimp","Diet Coke")

separator()

function myfun1(){}
myfun2 = function(){}
myfun3 = (dinner) => {console.log(dinner.main)}
myfun3(herDinner)
separator()
mydinner.order()
mydinner.pay()